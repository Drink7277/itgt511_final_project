﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerIdleState : State<Player>
{
    private static PlayerIdleState _instance;

    private PlayerIdleState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static PlayerIdleState Instance
    {
        get
        {
            if (_instance == null)
            {
                new PlayerIdleState();
            }

            return _instance;
        }
    }

    public override void EnterState(Player owner)
    {
        //Debug.Log("Entering Idle"); 
        Color color = new Color(1f, 1f, 1f);
        owner.ChangeBulletColor(color);
    }

    public override void ExitState(Player owner)
    {
        //Debug.Log("Exiting Idle");       
    }

    public override void UpdateState(Player owner)
    {
        //Debug.Log("Updating Idle");
        if (Input.GetKeyDown(KeyCode.E))
        {
            owner.stateMachine.ChangeState(PlayerAngryState.Instance);
        }
    }
}
