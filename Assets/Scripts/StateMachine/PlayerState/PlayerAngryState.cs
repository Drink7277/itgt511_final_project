﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAngryState : State<Player>
{
    private static PlayerAngryState _instance;

    private PlayerAngryState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static PlayerAngryState Instance
    {
        get
        {
            if (_instance == null)
            {
                new PlayerAngryState();
            }

            return _instance;
        }
    }

    public override void EnterState(Player owner)
    {
        //Debug.Log("Entering Angry");
        Color color = new Color(1f, 0.5f, 0.5f);
        owner.ChangeBulletColor(color);
    }

    public override void ExitState(Player owner)
    {
        //Debug.Log("Exiting Angry");
    }

    public override void UpdateState(Player owner)
    {
        //Debug.Log("Updating Angry");
        if (Input.GetKeyDown(KeyCode.E))
        {
            owner.stateMachine.ChangeState(PlayerInvisibleState.Instance);
        }
    }
}