﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerInvisibleState : State<Player>
{
    private static PlayerInvisibleState _instance;

    private PlayerInvisibleState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static PlayerInvisibleState Instance
    {
        get
        {
            if (_instance == null)
            {
                new PlayerInvisibleState();
            }

            return _instance;
        }
    }

    public override void EnterState(Player owner)
    {
        //Debug.Log("Entering Invisible");
        Color color = new Color(0.3f, 0.4f, 0.6f);
        owner.ChangeBulletColor(color);
    }

    public override void ExitState(Player owner)
    {
        //Debug.Log("Exiting Invisible");
    }

    public override void UpdateState(Player owner)
    {
        //Debug.Log("Updating Invisible");
        if (Input.GetKeyDown(KeyCode.E))
        {
            owner.stateMachine.ChangeState(PlayerIdleState.Instance);
        }
    }
}