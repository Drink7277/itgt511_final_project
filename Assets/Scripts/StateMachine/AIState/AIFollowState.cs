﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIFollowState : State<AI>
{
    private static AIFollowState _instance;

    private AIFollowState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static AIFollowState Instance
    {
        get
        {
            if (_instance == null)
            {
                new AIFollowState();
            }

            return _instance;
        }
    }

    public override void EnterState(AI owner)
    {
        //Debug.Log("Entering Follow");
        owner.ContinueMoving();
    }

    public override void ExitState(AI _owner)
    {
        //Debug.Log("Exiting Follow");
    }

    public override void UpdateState(AI owner)
    {
        //Debug.Log("Updating Follow");
        owner.MoveToTarget();
        if (!owner.IsTargetInRadius() || owner.IsTargetAngry())
            owner.stateMachine.ChangeState(AIIdleState.Instance);
    }
}