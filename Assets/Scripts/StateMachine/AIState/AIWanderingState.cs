﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWanderingState : State<AI>
{

    private static AIWanderingState _instance;

    private AIWanderingState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static AIWanderingState Instance
    {
        get
        {
            if (_instance == null)
            {
                new AIWanderingState();
            }

            return _instance;
        }
    }

    public override void EnterState(AI owner)
    {
        //Debug.Log("Entering Wandering");
        owner.ContinueMoving();
        owner.MoveTo(RandomDestination(owner));
    }

    public override void ExitState(AI _owner)
    {
        //Debug.Log("Exiting Wandering");
    }

    public override void UpdateState(AI owner)
    {
        //Debug.Log("Updating Wandering");
        if (owner.IsTargetInRadius())
            owner.stateMachine.ChangeState(AIIdleState.Instance);
        else if (owner.IsStandStill())
            owner.MoveTo(RandomDestination(owner));
    }

    private Vector3 RandomDestination(AI owner)
    {
        float distance = Random.Range(0,owner.lookRadius);
        float angle = Random.Range(0, 360);
        Vector3 vector = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward * distance;
        return owner.transform.position + vector;
    }
}
