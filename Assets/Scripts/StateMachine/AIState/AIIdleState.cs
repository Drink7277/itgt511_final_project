﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIIdleState : State<AI>
{
    private static AIIdleState _instance;

    private AIIdleState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static AIIdleState Instance
    {
        get
        {
            if (_instance == null)
            {
                new AIIdleState();
            }

            return _instance;
        }
    }

    public override void EnterState(AI owner)
    {
        //Debug.Log("Entering Idle");
        owner.StopMoving();
    }

    public override void ExitState(AI _owner)
    {
        //Debug.Log("Exiting Idle");
    }

    public override void UpdateState(AI owner)
    {
        //Debug.Log("Updating AIIdle");
        if (!owner.IsTargetInRadius())
            owner.stateMachine.ChangeState(AIWanderingState.Instance);
        else if (owner.IsTargetAngry())
            owner.stateMachine.ChangeState(AIFleeState.Instance);
        else
            owner.stateMachine.ChangeState(AIFollowState.Instance);

    }
            

}
