﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIFleeState : State<AI>
{

    private static AIFleeState _instance;

    private AIFleeState()
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static AIFleeState Instance
    {
        get
        {
            if (_instance == null)
            {
                new AIFleeState();
            }

            return _instance;
        }
    }

    public override void EnterState(AI owner)
    {
        //Debug.Log("Entering Flee");
        owner.ContinueMoving();
    }

    public override void ExitState(AI _owner)
    {
        //Debug.Log("Exiting Flee");
    }

    public override void UpdateState(AI owner)
    {
        //Debug.Log("Updating Flee");
        owner.MoveAwayFromTarget();
        if (!owner.IsTargetInRadius() || !owner.IsTargetAngry())
            owner.stateMachine.ChangeState(AIIdleState.Instance);
    }
}
