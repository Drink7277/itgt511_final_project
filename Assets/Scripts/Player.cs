﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public StateMachine<Player> stateMachine { get; set; }
    public float AngularSpeed = 120.0f;
    public GameObject Bullet;
    public uint maxBullet = 3;

    private GameObject[] ShootedBullet;
    private float verticalVelocity;
    private float gravity = 14.0f;
    private float jumpForce = 8.0f;
    private CharacterController controller;


    public bool IsAngry()
    {
        return stateMachine.currentState is PlayerAngryState;
    }

    public bool IsInvisible()
    {
        return stateMachine.currentState is PlayerInvisibleState;
    }

    public void ChangeBulletColor(Color color)
    {
        Bullet.GetComponent<Renderer>().material.color = color;
    }

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        stateMachine = new StateMachine<Player>(this);
        //stateMachine.ChangeState(PlayerIdleState.Instance);
        stateMachine.ChangeState(PlayerInvisibleState.Instance);
    }

    private void Update()
    {
        UpdateVerticalVelocity();
        UpadteGroundMovement();
        stateMachine.Update();

        UpdateAction();
    }

    private void UpdateAction()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            Debug.Log("Shoot-------------");
            //Debug.Log(Bullet.active);
            Shoot();
        }
    }

    private void Shoot()
    {
        Transform parent = GameObject.Find("BulletsPool").transform;
        GameObject new_bullet = Instantiate(Bullet, Bullet.transform.position , Bullet.transform.rotation, parent);
        new_bullet.SetActive(true);
    }

    private void UpdateVerticalVelocity()
    {
        if (controller.isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;

            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    verticalVelocity = jumpForce;

            //}

        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }
    }

    private void UpadteGroundMovement()
    {
        Vector3 moveVector = new Vector3(0, verticalVelocity, 0);
        moveVector.x = Input.GetAxis("Horizontal") * 5.0f;
        moveVector.y = verticalVelocity;
        moveVector.z = Input.GetAxis("Vertical") * 5.0f;

        controller.Move(moveVector * Time.deltaTime);
        UpdateRotation(moveVector);
    }

    private void UpdateRotation(Vector3 direction)
    {
        direction.y = 0;
        if (Mathf.Abs(direction.magnitude) < 0.001f)
            return;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), AngularSpeed * Time.deltaTime);
    }
}