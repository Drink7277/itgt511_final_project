﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float Speed = 3f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("Update from bullet");
        gameObject.transform.Translate(Vector3.forward * Speed * Time.deltaTime);		
	}

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("On trigger from bullet");
        Destroy(gameObject);
        if (other.GetComponentInParent<AI>())
            Destroy(other.gameObject);
    }
}
