﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{


    public float lookRadius = 6f;
    public StateMachine<AI> stateMachine { get; set; }

    private GameObject target;
    private NavMeshAgent agent;

    // Use this for initialization
    void Start()
    {
        target = GameObject.Find("Player");
        agent = GetComponent<NavMeshAgent>();

        stateMachine = new StateMachine<AI>(this);
        stateMachine.ChangeState(AIIdleState.Instance);
    }



    // Update is called once per frame
    void Update()
    {
        stateMachine.Update();
    }

    public bool IsTargetInRadius() {
        if (IsTargetInvisible()) return false;
        float distance = Vector3.Distance(target.transform.position, transform.position);
        return distance <= lookRadius;
    }

    public void MoveTo(Vector3 position) {
        agent.SetDestination(position);
    }

    public void MoveAwayFromTarget()
    {
        Vector3 position = 2 * transform.position - target.transform.position;
        MoveTo(position);
    }


    public void MoveToTarget() {
        MoveTo(target.transform.position);
    }

    public bool IsTargetAngry()
    {
        return target.GetComponent<Player>().IsAngry();
    }

    public bool IsTargetInvisible()
    {
        return target.GetComponent<Player>().IsInvisible();
    }

    public void StopMoving() {
        agent.isStopped = true;
    }

    public void ContinueMoving()
    {
        agent.isStopped = false;
    }

    public bool IsStandStill()
    {
        return agent.velocity == Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }




}


  



      

   



